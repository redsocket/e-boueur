
menu();

function menu(){
let gamee = new Phaser.Game(1568, 896, Phaser.AUTO, ' ', { preload: preload, create: create, update: update});

function preload() {

    gamee.load.spritesheet('buttonplay', 'assets/play.png', 400, 200);
    gamee.load.spritesheet('buttonfullscreen', 'assets/fullscreen.png', 75, 75);
    gamee.load.image('background','assets/background_menu.png');

}

let group;
var KeyF;


function create() {

    
    gamee.scale.startFullScreen(true);
    
    gamee.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    gamee.stage.backgroundColor = '#000000';


    gamee.add.tileSprite(0, 0, 1568, 896, 'background');
    

    group = gamee.add.group();

    let buttonplay = gamee.make.button(gamee.world.centerX - 120, 200, 'buttonplay', removeGroup, this, 2, 1, 0);

    let buttonfullscreen = gamee.make.button(gamee.world.centerX - 770, 10, 'buttonfullscreen', gofullscreen, this, 2, 1, 0);
    
    KeyF = gamee.input.keyboard.addKey(Phaser.Keyboard.F);
    KeyF.onDown.add(gofullscreen, this);
    gamee.input.keyboard.removeKeyCapture(Phaser.Keyboard.F);

    group.add(buttonplay);
    group.add(buttonfullscreen);

}


function gofullscreen() {

    if (gamee.scale.isFullScreen)
    {
        gamee.scale.stopFullScreen();
    }
    else
    {
        gamee.scale.startFullScreen(false);
    }

}

function removeGroup() {

    if (gamee.scale.isFullScreen)
    {
        gamee.scale.stopFullScreen();
    }
    gamee.world.remove(group);
    group.destroy();
    gamee.destroy();
    jeux();

}


function update(){

    if (gamee.input.activePointer.withinGame)
    {
        gamee.input.enabled = true;
    }
    else
    {
        gamee.input.enabled = false;
    }

}

}