
function jeux () {

let game = new Phaser.Game(1568, 896, Phaser.AUTO, '', { preload: preload, create: create, update: update });

    let height=1568;
    let width=896;

    let map = null;
    let layer = null;
    let layerRoute = null;
    let layerCoin = null;
    let layerMaison=null;
    let car;

    let safetile = -1;
    let gridsize = 32;

    let speed = 150;
    let threshold = 5;
    let turnSpeed = 150;

    let marker = new Phaser.Point();
    let turnPoint = new Phaser.Point();

    let directions = [null, null, null, null, null];
    let opposites = [Phaser.NONE, Phaser.RIGHT, Phaser.LEFT, Phaser.DOWN, Phaser.UP];

    let current = Phaser.UP;
    let turning = Phaser.NONE;

    let cursors;

    let coin;
    let score=0;
    let scoreText;

    let scoreTime;
    let time;
    
    let compteur=0;
    let blockTurn=10;

    var KeyF;


    function preload() {
    //prechargement du tilemap
    game.load.tilemap('map', "/assets/city.json", null, Phaser.Tilemap.TILED_JSON);

    //Prechargement des tileset qui composent la map
    game.load.image('routes','assets/tilesetRoute.png');
    game.load.image('lac','assets/tilesetlac.png');
    game.load.image('gazon','assets/gazon.png');
    game.load.image('maison','assets/tilesetmaison.png')

    //Prechargement image
    game.load.image('camion','assets/camion.png');
    game.load.image('coin1','assets/Déchet1.png');
    game.load.image('coin2','assets/Déchet2.png');
    game.load.image('coin3','assets/Déchet3.png');
    game.load.image('coin4','assets/Déchet4.png');
    game.load.image('bouclier','assets/bouclier.png');
}

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.stage.backgroundColor = '#0000000';


    //on passe de plein ecran

    game.scale.startFullScreen(true);
 

    KeyF = game.input.keyboard.addKey(Phaser.Keyboard.F);
    KeyF.onDown.add(gofullscreen, this);
    game.input.keyboard.removeKeyCapture(Phaser.Keyboard.F);
    

    //on cree une tilemap
    map = game.add.tilemap('map');
    //On ajoute tout les tileset contenue dans la map
    map.addTilesetImage('Route','routes');
    map.addTilesetImage('Lac','lac');
    map.addTilesetImage('Gazon','gazon');
    //en parametre d'abord le nom du tileset dans tiled puis le nom de l'image composant le tileset qu'on a precharger dans la fonction preload
    map.addTilesetImage('tilesetmaison','maison')

    //Layer contenant tout se qui bloque
    layer=map.createLayer('Ground');
    //Layer comprenant les routes
    layerRoute = map.createLayer('Route');
    //layer contenant les maisons
    layerMaison = map.createLayer('Maison');

    layer.resizeWorld();
    layerRoute.resizeWorld();

    //On cree la collision sur layer
    map.setCollisionByExclusion([], true, layer) 

    car = game.add.sprite(48, 48, 'camion');
    car.anchor.set(0.5);

    game.physics.arcade.enable(car);

    //On cree le groupe contenant toutes les pieces
    coin=game.add.physicsGroup();
    coin.enableBody=true;
    


    //On cree des pieces sur chaque case de route
    for(let x=0;x<height;x=x+32){
        for(let y=0;y<height;y=y+32){
            if(map.hasTile(x/32,y/32,layerRoute)){
                createcoin(x,y);
            }
        }
    }

    //Cursors contenant les touches pressés
    cursors = game.input.keyboard.createCursorKeys();

    //On bouge vers le bas des le debut
    //move(Phaser.DOWN);
    scoreText = game.add.text(1300, 10, 'score: 0', { fontSize: '35px', fill: '#FFF'}); // Gerer position et taille du score.

    //creation du timer 
    timer = game.time.create(false);
    timer.loop(300000, GameOver,game); // valeur du timer
    timer.start();
    
    
    let progress = game.add.image(0, 0, "bar");progress.width = 0;progress.initialWidth = 300


    //bouclier
}

function gofullscreen() {

    if (game.scale.isFullScreen)
    {
        game.scale.stopFullScreen();
    }
    else
    {
        game.scale.startFullScreen(false);
    }

}

//Fonction qui crée les pieces
function  createcoin(x,y) {
    let c = game.add.sprite(x,y, 'coin'+ Math.round((Math.random()*3)+1));
    coin.add(c);
}


//Fonction appelé quand une piece est touché
function catchCoin(camion, coin){
    
    coin.kill();
    score += 10; // a changer si besoin pour le score
    scoreText.setText('Score: ' + score);
    //console.log(score);
}

function checkKeys() {

    if (cursors.left.isDown && current !== Phaser.LEFT)
    {
        checkDirection(Phaser.LEFT);
    }
    else if (cursors.right.isDown && current !== Phaser.RIGHT)
    {
        checkDirection(Phaser.RIGHT);
    }
    else if (cursors.up.isDown && current !== Phaser.UP)
    {
        checkDirection(Phaser.UP);
    }
    else if (cursors.down.isDown && current !== Phaser.DOWN)
    {
        checkDirection(Phaser.DOWN);
    }
    else
    {
        //  This forces them to hold the key down to turn the corner
        turning = Phaser.NONE;
    }

}

function checkDirection(turnTo) {

    if (turning === turnTo || directions[turnTo] === null || directions[turnTo].index !== safetile)
    {
        //  Invalid direction if they're already set to turn that way
        //  Or there is no tile there, or the tile isn't index a floor tile
        return;
    }

    //  Check if they want to turn around and can
    if (current === opposites[turnTo])
    {
        move(turnTo);
    }
    else
    {
        turning = turnTo;

        turnPoint.x = (marker.x * gridsize) + (gridsize / 2);
        turnPoint.y = (marker.y * gridsize) + (gridsize / 2);
    }
}

function turn() {

    let cx = Math.floor(car.position.x);
    let cy = Math.floor(car.position.y);

    //  This needs a threshold, because at high speeds you can't turn because the coordinates skip past
    if ((!game.math.fuzzyEqual(cx, turnPoint.x, threshold)) || (!game.math.fuzzyEqual(cy, turnPoint.y, threshold)))
    {   
        return false;
    }
    car.x = turnPoint.x;
    car.y = turnPoint.y;

    car.body.reset(turnPoint.x, turnPoint.y);

    move(turning);

    turning = Phaser.NONE;

    return true;

}

function move(direction) {

    let speedtmp=speed;

    compteur=blockTurn;
    
    if (direction === Phaser.LEFT || direction === Phaser.UP)
    {
           speedtmp = -speedtmp;
       }
   
    if (direction === Phaser.LEFT || direction === Phaser.RIGHT)
    {
        car.body.velocity.x = speedtmp;
    }
    else
    {
        car.body.velocity.y = speedtmp;
    }

    game.add.tween(car).to( { angle: getAngle(direction) }, turnSpeed, "Linear", true);
  
    current = direction;
}

function getAngle(to) {

    //  About-face?
    if (current === opposites[to])
    {
        return "180";
    }

    if ((current === Phaser.UP && to === Phaser.LEFT) ||
        (current === Phaser.DOWN && to === Phaser.RIGHT) ||
        (current === Phaser.LEFT && to === Phaser.DOWN) ||
        (current === Phaser.RIGHT && to === Phaser.UP))
    {
        return "-90";
    }

    return "90";

}

//fin du jeu


function update() {

    scoreTimer=game.debug.text('Timer: ' + Math.round(timer.duration.toFixed(0)/1000), 1300, 70);
    //on cree la collision entre le camion et la layer contenant le sol
    game.physics.arcade.collide(car, layer);

    //On appelle la fonction catchCoin quand le camion touche une piece
    game.physics.arcade.overlap(car,coin,catchCoin,null,game);


    //compteur empechant le spam de touche et qui empeche la desorientation du sprite du camion
    //On empeche le camion de tourner si le compteur n'est pas inferieur a 0
    if(compteur<0){
        marker.x = game.math.snapToFloor(Math.floor(car.position.x), gridsize) / gridsize;
        marker.y = game.math.snapToFloor(Math.floor(car.position.y), gridsize) / gridsize;

        let i = layer.index;
        let x = marker.x;
        let y = marker.y;

        directions[1] = map.getTileLeft(i, x, y);
        directions[2] = map.getTileRight(i, x, y);
        directions[3] = map.getTileAbove(i, x, y);
        directions[4] = map.getTileBelow(i, x, y);

        checkKeys();

        if (turning !== Phaser.NONE)
        {   
            turn();
        }

    }
    //On decremente le compteur a chaque update
    compteur--;

   
    if(score==200){// conditions de victoire ou defaite pour appeler game over
        if (game.scale.isFullScreen)
        {
            game.scale.stopFullScreen();
        }    
    GameOver();
    game.destroy();
    }
    }
}