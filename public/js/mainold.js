var game = new Phaser.Game(1920, 1080, Phaser.AUTO, '', { preload: preload, create: create, update: update });

console.log("Start game...");


function preload() {
    //prechargement du tilemap
    game.load.tilemap('map', "/assets/city.json", null, Phaser.Tilemap.TILED_JSON);

    //Prechargement des tileset qui composent la map
    game.load.image('routes','assets/tilesetRoute.png');
    game.load.image('lac','assets/tilesetlac.png');
    game.load.image('gazon','assets/gazon.png');

    //Prechargement image
    game.load.image('camion','assets/camion.png')
    
}

let map;

let safetile=1;
let gridsize=32

let groundLayer;
let collisionLayer;

let sprite;
let cursors;

let speed=150;
let threshold = 3;
let turnSpeed = 150;

let marker = new Phaser.Point();
let turnPoint = new Phaser.Point();

let directions =[null, null, null, null, null];
let opposites = [Phaser.NONE, Phaser.RIGHT, Phaser.LEFT, Phaser.DOWN, Phaser.UP ];

let current=Phaser.UP;
let turning=Phaser.None



function create() {
    //Creation de la map
    map=game.add.tilemap('map');

    //On doit ajouter le tileset qu'on a créé avec a gauche le nom du tileset .json et a droite le nom du png composant le tileset chargé dans la fonction preload
    map.addTilesetImage('Route','routes');
    map.addTilesetImage('Lac','lac');
    map.addTilesetImage('Gazon','gazon');

    //Nom du calque super important sans ca rien ne s'affiche
    //Creation du calque collision qui comporte tout ce qui fait collision (gazon, maison etc..)
    collisionLayer=map.createLayer('Collision');
    //On creer la collision
    map.setCollisionBetween(1, 999, true, collisionLayer);
    game.physics.enable(collisionLayer);

    //creation du calque Ground qui comprend toutes les routes, on peut se deplacer dessus
    groundLayer=map.createLayer('Ground');
    game.physics.enable(groundLayer);


    //Ajout du sprite du joueur
    p=game.add.sprite(48,48,'camion');
    p.anchor.setTo(0.5,0.5);

    //Affichage du sprite
    game.physics.enable(p);
    p.body.collideWorldBounds = true;


    //recuperation de la touche appuyée
    cursors = game.input.keyboard.createCursorKeys();

    move(Phaser.DOWN);
}



function update() {
    //Verification des collisions
    game.physics.arcade.collide(p, collisionLayer);

    if (cursors.left.isDown)
    {
        p.body.angularVelocity = -200;
    }
    else if (cursors.right.isDown)
    {
        p.body.angularVelocity = 200;
    }

    if (cursors.up.isDown)
    {
        p.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(p.angle, 300));
    }
}
